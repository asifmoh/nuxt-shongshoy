import { Post } from "~~/types/post";

export default () => {
  const config = useRuntimeConfig();
  const WP_URL: string = config.wpUri;

  const get = async <T>(endpoint: string) => {
    const response = await useFetch<T>(`${WP_URL}/wp-json/wp/v2/${endpoint}`);
    if (typeof response === "string") {
      return JSON.parse(decodeURIComponent(response));
    }
    return response;
  };

  const getPosts = async (
    category?: number,
    page: number = 1,
    perPage: number = 50,
    fields: string = "author,id,excerpt,title,link,slug,date"
  ) => {
    let query: string = `posts?page=${page}&per_page=${perPage}&_embed=1`;
    if (category) {
      query += `&categories=${category}`;
    }
    return get<Post[]>(query);
  };

  const getPost = async (slug: string) => {
    
    return get<Post[]>(`posts?slug=${slug}&_embed=1`);
  };

  const getCategories = async (fields: string = "name,slug,count") => {
    return get<any>(`categories`);
  };

  const getCategory = async (slug: string) => {
    return get<any>(`categories?slug=${slug}`);
  };

  const getTags = async (fields: string = "name,slug,count") => {
    return get<any>(`tags?per_page=100`);
  };

  return {
    get,
    getPosts,
    getPost,
    getCategories,
    getCategory,
    getTags,
  };
};
