export default defineNuxtConfig({
  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width,initial-scale=1",
      title: "সংশয় - চিন্তার মুক্তির আন্দোলন",
      titleTemplate: "%s - সংশয় - চিন্তার মুক্তির আন্দোলন",
      meta: [
              { name: "description", content: "সংশয় - চিন্তার মুক্তির আন্দোলন" },
              { name: "robots", content: "noindex" }
             ],

      link: [
        { rel: "icon", type: "image/x-icon", href: "/images/favicon.ico" },
        { rel: 'apple-touch-icon', sizes: '180x180', href: '/images/apple-touch-icon.png' },
        { rel: 'icon', sizes: '32x32', href: '/images/favicon-32x32.png' },
        { rel: 'icon', sizes: '16x16', href: '/images/favicon-16x16.png' },
        { rel: 'manifest', href: '/site.webmanifest' },
        { rel: 'text/javascript', href: '/pwabuilder-sw.js' },
        { rel: 'txt', href: '/robots.txt' }

      ]       
    },
    
  },
  modules: ["@nuxtjs/tailwindcss"],
  runtimeConfig: {
    public: {
      wpUri: process.env.WP_URI,
    },
    proxy: {
      '/api/': { target: 'https://www.shongshoy.com/', pathRewrite: {'^/api/': ''}, changeOrigin: true }
    },
  }
});

